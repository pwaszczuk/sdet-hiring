import comment from "../fixtures/comments.json";

Cypress.Commands.add('clickElement', function (cy_selector) {
    cy.get(`[data-cy="${cy_selector}"]`).click();
});

Cypress.Commands.add('checkRowByIndex', function (row_index, comment_index) {
    cy.get('tbody tr').eq(row_index).within(() => {
        cy.get('td').eq(0).should('have.text', comment[comment_index].id);
        cy.get('td').eq(2).should('have.text', comment[comment_index].name);
        cy.get('td').eq(3).should('have.text', comment[comment_index].email);
        cy.get('td').eq(4).should('have.text', comment[comment_index].content);
    });
});