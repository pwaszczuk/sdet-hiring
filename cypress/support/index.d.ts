/// <reference types="cypress" />
declare namespace Cypress {
    interface Chainable<Subject> {
        /**
         * Get element by data-cy selector
         * @example
         * cy.clickElement('save');
         */
        clickElement(cy_selector: string): Chainable<any>;

        /**
         * Check row with comments,
         * based on fixture comments.json
         * @example
         * cy.checkRowByIndex(1, 1);
         */
        checkRowByIndex(row_index: number, comment_index: number): Chainable<any>;
    }
}
