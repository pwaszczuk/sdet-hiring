/// <reference types="cypress" />
require('dotenv').config()

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => {
    config.env.playername = process.env.PLAYER_NAME
    return config
}
