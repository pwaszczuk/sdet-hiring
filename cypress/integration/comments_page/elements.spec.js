describe('Check comments elements', function () {
    before(function () {
        cy.visit('/comments');
        cy.get('tbody').should('be.visible');
    });

    it('Check header', function () {
        cy.get('table thead tr').first().within(() => {
            cy.get('th').eq(0).should('contain', 'IDs');
            cy.get('th').eq(1).should('contain', 'User');
            cy.get('th').eq(2).should('contain', 'Content');
        });
    });

    it('Check sorters', function () {
        cy.get('table thead tr').eq(1).within(() => {
            cy.get('th').eq(0).should('contain', 'Post ID');
            cy.get('th').eq(1).should('contain', 'ID');
            cy.get('th').eq(2).should('contain', 'Name');
            cy.get('th').eq(3).should('contain', 'Email');
            cy.get('th').eq(4).should('contain', 'Contents');
        });
    });

    it('Check comments number', function () {
        cy.get('tbody tr').should('have.length', '500');
    });
});