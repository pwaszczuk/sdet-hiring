describe('Check comments page actions', function () {
    beforeEach(function () {
        cy.visit('/comments');
        cy.get('tbody').should('be.visible');
    });

    it('Sort by id', function () {
        cy.get('table thead tr').eq(1).within(() => {
            cy.get('th').first().click()
            cy.get('th span').first().should('not.be.empty')
        });
        cy.checkRowByIndex(0, 0);
    });

    it('Sort by email', function () {
        cy.get('table thead tr').eq(1).within(() => {
            cy.get('th').eq(3).click()
            cy.get('th span').eq(3).should('not.be.empty')
        });
        cy.checkRowByIndex(0, 1);
    });
});