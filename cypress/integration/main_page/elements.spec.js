describe('Check main page actions', function () {
    before(function () {
        cy.visit('');
    });

    it('Check header', function () {
        cy.get('.App-header').within(() => {
            cy.get('h2').should('have.text', 'Welcome to online casino');
        });
    });

    it('Check user name introduction fields', function () {
        cy.get('[data-cy="intro"]').should('have.text', 'Introduce yourself!');
        cy.get('input').should('have.attr', 'type');
        cy.get('[data-cy="save"]').should('have.text', 'Save name');
    });

    it('Check if comments page is alive', function () {
        cy.get('[data-cy="comments"]').invoke('attr', 'href')
            .then((url) => {
                cy.request(url).its('status').should('eq', 200);
            });
    });
});