const host = Cypress.config('baseUrl');

describe('Check main page actions', function () {
    beforeEach(function () {
        cy.visit('');
    });

    it('Set name', function () {
        const name = Cypress.env('playername');
        cy.get('input').type(name);
        cy.clickElement('save');
        cy.get('[data-cy="welcome"]').should('have.text', `Welcome ${name}`);
    });

    it('Roll the dice', function () {
        cy.get('[data-cy="state"]').should('have.text', 'You must roll the dice first');
        cy.clickElement('roll');
        cy.get('[data-cy="state"]').then(($state) => {
            if ($state.text().includes('6')) {
                cy.get('[data-cy="response"]').should('have.text', 'Roll one more six in a row to win');
            } else {
                cy.get('[data-cy="response"]').should('have.text', '').and('not.be.visible');
                cy.get('[data-cy="state"]').should('contain', 'Current roll:');
            }
        });
    });

    it('Open comments page', function () {
        cy.clickElement('comments');
        cy.url().should('eq', host + 'comments');
        cy.get('table').should('be.visible');
    });
});