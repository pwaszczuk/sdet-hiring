# Awesome Casino App

This is an awesome Casino App!

## Task

The objective of this task is to find as many bugs as possible in the app provided. You can achieve this task by using Cypress to automate the testing. Once the bugs have been found, you should generate a report with your findings, as well as supply us with your code.

## Items we look for
- Thorough investigation of the app
- Attention to detail
- Looking for edge cases
- Proper bug reporting
- Demonstrating best practices when writing your code

## Requirements
We will evaluate your code from two perspectives. Did you meet the requirements and was your code high quality. Obviously, there is some ambiguity as the requirements are loose and no coding standards have been provided, so please ask questions if you have any but please take the liberty to be creative in your solution. As for coding standards, we prioritize consistency, readability, and maintainability when doing our evaluation. Also, we would like to see your Git workflow. This task should take up to 4 hours to complete.

## Code Delivery
To get started please fork this repository into your own personal Bitbucket account (Free if you don’t have one) for easy code submission (https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/). We ask when forking you make your repository public and as part of the submission, you can share your repo URL.

## Available Scripts

In the project directory, you can run:

### `npm install`

It installs project dependencies.

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `cypress:open`

Runs clean installation of the Cypress Test Runner. Feel free to configure it to match your approach.

### `cypress:reported:run`
Runs all Cypress tests in a headless mode and returns report in html, which can be find in 
cypress/reports/html/output.html